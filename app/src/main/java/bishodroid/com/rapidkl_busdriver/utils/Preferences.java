package bishodroid.com.rapidkl_busdriver.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import bishodroid.com.rapidkl_busdriver.pojo.Driver;

/**
 * Created by user on 21/2/2016.
 */
public class Preferences {

    private static Preferences singelton;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private Context context;
    private Gson gson;


    private Preferences(Context context) {
        this.context = context;
        this.prefs = context.getSharedPreferences(Constants.APP_SETTINGS, Context.MODE_PRIVATE);
        editor = prefs.edit();
        gson = new Gson();
    }


    /**
     * Using singleton Preferences object to save and retrieve settings
     *
     * @param context
     * @return singleton Preferences object
     */
    public static synchronized Preferences getInstance(Context context) {
        if (singelton == null) {
            singelton = new Preferences(context);
        }
        return singelton;
    }

    /**
     * To update boolean settings
     *
     * @param key   - name of boolean to update
     * @param value - either true or false
     */
    public void putBoolean(String key, boolean value) {
        editor.putBoolean(key, value).commit();
    }

    /**
     * Getting boolean settings
     *
     * @param key          - name of boolean settings
     * @param defaultValue - default value if nothing was found
     * @return true or false
     */
    public boolean getBoolean(String key, boolean defaultValue) {
        return prefs.getBoolean(key, defaultValue);
    }

    /**
     * To Update String settings
     *
     * @param key   - name of the string setting
     * @param value - any string
     */
    public void putString(String key, String value) {
        editor.putString(key, value).commit();
    }

    /**
     * Getting String settings
     *
     * @param key      - name of the setting
     * @param defValue
     * @return the preference saved by user
     */
    public String getString(String key, String defValue) {
        return prefs.getString(key, defValue);
    }

    /**
     * Get the driver object;
     *
     * @param key
     * @param defaultVal
     * @return driver object
     */
    public Driver getDriver(String key, String defaultVal) {
        String driverJson = prefs.getString(Constants.DRIVER_KEY, defaultVal);
        return gson.fromJson(driverJson, Driver.class);
    }

    /**
     * Puts new driver to prefs
     *
     * @param key
     * @param driver
     */
    public void putDriver(String key, Driver driver) {
        String driverJson = gson.toJson(driver);
        editor.putString(Constants.DRIVER_KEY, driverJson).commit();
    }

}
