// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package bishodroid.com.rapidkl_busdriver.utils;

import android.content.Context;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectionsChecker {

    private static ConnectivityManager connectivityManager;
    private static Context context;
    private static LocationManager locationManager;
    private static NetworkInfo networkInfo;
    private static ConnectionsChecker singelton;

    private ConnectionsChecker(Context context1) {
        context = context1;
    }

    public static ConnectionsChecker getInstance(Context context1) {
        if (singelton == null) {
            singelton = new ConnectionsChecker(context1);
        }
        return singelton;
    }

    public static boolean isGpsAvailable() {
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static boolean isNetworkAvailable() {
        connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}
