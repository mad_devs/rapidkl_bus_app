// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package bishodroid.com.rapidkl_busdriver.utils;

import android.content.Context;

import java.util.Arrays;
import java.util.List;

import bishodroid.com.rapidkl_busdriver.R;

public class Utils {

    public static List getAreas(Context context) {
        return Arrays.asList(context.getResources().getStringArray(R.array.areas));
    }

    public static List getRoutesForArea(String area, Context context) {
        switch (area) {
            case Constants.AREA_AMPANG:
                return Arrays.asList(context.getResources().getStringArray(R.array.buses_ampang));
            case Constants.AREA_IPOH:
                return Arrays.asList(context.getResources().getStringArray(R.array.buses_ipoh));
        }
        return null;
    }
}
