// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package bishodroid.com.rapidkl_busdriver.utils;


public class Constants {

    public static final String APP_DIR_NAME = "/rapidKL";
    //fonts paths
    public static final String FONTS_DOTS = "fonts/dots.ttf";
    public static final String APP_SETTINGS = "SETTINGS";

    public static final String AREA_AMPANG = "AMPANG";
    public static final String AREA_IPOH = "IPOH";

    public static final String AREA_KEY = "key-area";
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 6000L;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    public static final String FILE_NAME = "locationUpdates.txt";
    public static final String LAST_UPDATED_TIME_STRING_KEY = "last-updated-time-string-key";
    public static final String LOCATION_KEY = "location-key";
    public static final int NAME_LENGTH = 6;
    public static final String REQUESTING_LOCATION_UPDATE_KEY = "requesting-location-updates-key";
    public static final String ROUTE_KEY = "key-route";
    public static final String UPDATES_DIR_PATH = "/RapidBus/";


    public static final String DRIVER_KEY = "DRIVER_KEY";
    public static final String EXTRA_DRIVER = "DRIVER_EXTRA";
}
