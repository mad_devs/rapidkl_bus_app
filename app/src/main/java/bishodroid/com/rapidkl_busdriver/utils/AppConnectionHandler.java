package bishodroid.com.rapidkl_busdriver.utils;

import android.util.Log;

import de.tavendo.autobahn.WebSocketConnectionHandler;

/**
 * Created by Bisho on 21/2/2016.
 */
public class AppConnectionHandler extends WebSocketConnectionHandler {

    private static final String TAG = "WEBSOCKETHANDLER";

    @Override
    public void onOpen() {
        Log.d(TAG, "Connected to WS");
    }

    @Override
    public void onClose(int code, String reason) {
        super.onClose(code, reason);
    }

    @Override
    public void onTextMessage(String payload) {
        super.onTextMessage(payload);
        Log.d(TAG, "Connected to WS " + payload);
    }
}
