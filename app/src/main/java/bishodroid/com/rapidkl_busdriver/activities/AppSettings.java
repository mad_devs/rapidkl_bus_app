package bishodroid.com.rapidkl_busdriver.activities;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import bishodroid.com.rapidkl_busdriver.fragments.UserSettings;

/**
 * Created by Bisho on 21/2/2016.
 */
public class AppSettings extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new UserSettings()).commit();
    }

}
