// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package bishodroid.com.rapidkl_busdriver.activities;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import bishodroid.com.rapidkl_busdriver.R;
import bishodroid.com.rapidkl_busdriver.pojo.Driver;
import bishodroid.com.rapidkl_busdriver.services.LocationUpdateService;
import bishodroid.com.rapidkl_busdriver.utils.ConnectionsChecker;
import bishodroid.com.rapidkl_busdriver.utils.Constants;
import bishodroid.com.rapidkl_busdriver.utils.Dialogs;
import bishodroid.com.rapidkl_busdriver.utils.Preferences;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "Bus Driver";
    private static final int PREF_CODE = 007;
    private static final int PICK_PHOTO = 1;
    private TextView area;
    private ImageView bus;
    private CircleImageView driverPic;
    private TextView route;
    private TextView name;
    private Button startStop;
    private CoordinatorLayout coordinatorLayout;
    private Toolbar toolbar;
    private LocationUpdateService service;
    private boolean started;
    private String updateTime;
    private Driver driver;
    private AQuery aq;
    private BroadcastReceiver broadcastReceiver;
    private Preferences prefs;
    private ConnectionsChecker connection;
    private Context context;
    private Typeface font;
    private Uri outputFileUri;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_main);
        context = this;
        prefs = Preferences.getInstance(getApplicationContext());
        aq = new AQuery(getApplicationContext());
        connection = ConnectionsChecker.getInstance(getApplicationContext());
        installNetworkListener();
        font = Typeface.createFromAsset(getAssets(), Constants.FONTS_DOTS);
        initGui();
        started = false;
        updateTime = "";
        startStop.setOnClickListener(this);
        driverPic.setOnClickListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateUi();
        installNetworkListener();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
//        unregisterReceiver(broadcastReceiver);
    }

    /**
     * To initialize the GUI
     */
    private void initGui() {

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator);
        startStop = (Button) findViewById(R.id.btn_start);
        area = (TextView) findViewById(R.id.main_area_label);
        area.setTypeface(font);
        area.setTextColor(getResources().getColor(R.color.colorOrange));

        route = (TextView) findViewById(R.id.main_bus_route);
        route.setTypeface(font);
        route.setTextColor(Color.RED);

        name = (TextView) findViewById(R.id.driver_name);
//        bus = (ImageView) findViewById(R.id.bus_image);
        driverPic = (CircleImageView) findViewById(R.id.driver_pic);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(R.string.app_name);
        toolbar.setLogo(R.mipmap.ic_launcher);
        toolbar.setCollapsible(true);

        updateUi();

    }

    private void setButtonState() {
        if (started) {
            startStop.setText(getResources().getString(R.string.btn_txt_stop));
        } else {
            startStop.setText(getResources().getString(R.string.btn_txt_start));
        }
    }

    /**
     * To continously check for the internet connectivity and notify
     * the user when disconnected
     */
    private void installNetworkListener() {
        if (broadcastReceiver == null) {
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    Bundle extras = intent.getExtras();
                    NetworkInfo info = extras.getParcelable("networkInfo");
                    NetworkInfo.State network = info.getState();
                    Log.d("NETWORK", info.toString() + " " + network.toString());
                    if (network != NetworkInfo.State.CONNECTED) {
                        Snackbar.make(coordinatorLayout, R.string.title_enable_internet, Snackbar.LENGTH_INDEFINITE)
                                .setAction(R.string.reconnect, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.colorOrange))
                                .show();
                    }
                }
            };
            final IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            registerReceiver(broadcastReceiver, intentFilter);
        }
    }

    private void updateUi() {
        driver = prefs.getDriver(Constants.DRIVER_KEY, null);
        name.setText(driver.getName());
        area.setText(driver.getArea());
        route.setText(driver.getRoute());
        if (ConnectionsChecker.isNetworkAvailable())
            aq.id(driverPic).image(driver.getPhotoUrl());
        else
            driverPic.setImageResource(R.drawable.driver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivityForResult(new Intent(this, AppSettings.class), PREF_CODE);
        }

        return super.onOptionsItemSelected(item);
    }

    private void openImageIntent() {

// Determine Uri of camera image to save.
        final File root = new File(Environment.getExternalStorageDirectory() + File.separator + Constants.APP_DIR_NAME + File.separator);
        root.mkdirs();
        final String fname = "images";
        final File sdImageMainDirectory = new File(root, fname);
        outputFileUri = Uri.fromFile(sdImageMainDirectory);

        // Camera.
        final List<Intent> cameraIntents = new ArrayList<>();
        final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            cameraIntents.add(intent);
        }

        // Filesystem.
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, getResources().getString(R.string.choose_source));

        // Add the camera options.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));

        startActivityForResult(chooserIntent, PICK_PHOTO);
    }

    private void handleStart() {
        if (!started) {
            if (!ConnectionsChecker.isNetworkAvailable()) {
                Dialogs.showNetworkDialog(context);
                return;
            } else if (!ConnectionsChecker.isGpsAvailable()) {
                Dialogs.showGpsDialog(context);
                return;
            } else {
                startService(new Intent(getBaseContext(), LocationUpdateService.class));
                started = true;
                setButtonState();
            }
        } else {
            stopService(new Intent(getBaseContext(), LocationUpdateService.class));
            started = false;
            setButtonState();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case PREF_CODE:
                //TODO show something
                break;
            case RESULT_OK:
                if (requestCode == PICK_PHOTO) {
                    final boolean isCamera;
                    if (data == null) {
                        isCamera = true;
                    } else {
                        final String action = data.getAction();
                        isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
                    }

                    Uri selectedImageUri;
                    if (isCamera) {
                        selectedImageUri = outputFileUri;
                        driverPic.setImageURI(selectedImageUri);
                    } else {
                        selectedImageUri = data.getData();
                        driverPic.setImageURI(selectedImageUri);
                    }
                }
                break;
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_start:
                handleStart();
                break;
            case R.id.driver_pic:
//                openImageIntent();
                break;
        }
    }
}
