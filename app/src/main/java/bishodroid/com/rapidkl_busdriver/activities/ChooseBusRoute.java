// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package bishodroid.com.rapidkl_busdriver.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.util.List;

import bishodroid.com.rapidkl_busdriver.R;
import bishodroid.com.rapidkl_busdriver.pojo.Driver;
import bishodroid.com.rapidkl_busdriver.utils.ConnectionsChecker;
import bishodroid.com.rapidkl_busdriver.utils.Constants;
import bishodroid.com.rapidkl_busdriver.utils.Preferences;
import bishodroid.com.rapidkl_busdriver.utils.Utils;

// Referenced classes of package bishodroid.com.rapidbus.activities:
//            MainActivity

public class ChooseBusRoute extends AppCompatActivity
        implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private Spinner area;
    private ConnectionsChecker connectionsChecker;
    private TextInputLayout driverName;
    private TextWatcher myTextWatcher;
    private Spinner route;
    private Button submit;
    private Preferences prefs;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_choose_bus_route);
        connectionsChecker = ConnectionsChecker.getInstance(getApplicationContext());
        prefs = Preferences.getInstance(getApplicationContext());
        initGui();
    }

    /**
     * To initialize GUI
     */
    private void initGui() {
        submit = (Button) findViewById(R.id.btn_submit);
        submit.setEnabled(true);
        submit.setOnClickListener(this);
        myTextWatcher = new TextWatcher() {

            public void afterTextChanged(Editable editable) {
            }

            public void beforeTextChanged(CharSequence charsequence, int i, int j, int k) {
            }

            public void onTextChanged(CharSequence charsequence, int i, int j, int k) {
                if (charsequence.length() > 6) {
                    area.setEnabled(true);
                }
            }

        };
        driverName = (TextInputLayout) findViewById(R.id.driverNameWrapper);
        driverName.getEditText().addTextChangedListener(myTextWatcher);
        area = (Spinner) findViewById(R.id.choose_area);
        area.setEnabled(false);
        area.setAdapter(simpleSpinnerAdapter(Utils.getAreas(getApplicationContext())));
        area.setOnItemSelectedListener(this);
        route = (Spinner) findViewById(R.id.choose_route);
        route.setEnabled(true);
        route.setAdapter(simpleSpinnerAdapter(Utils.getRoutesForArea("AMPANG", getApplicationContext())));
        route.setOnItemSelectedListener(this);
    }

    private ArrayAdapter simpleSpinnerAdapter(List list) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        return adapter;
    }

    private boolean validName(String s) {
        return s.length() > 6;
    }

    /**
     * Create driver
     *
     * @return new Driver
     */
    private Driver createDriver() {
        Driver driver = new Driver();
        driver.setName(driverName.getEditText().getText().toString());
        driver.setArea(area.getSelectedItem().toString());
        driver.setRoute(route.getSelectedItem().toString());
        driver.setPhotoUrl("https://cdn0.iconfinder.com/data/icons/activities-flat-colorful/2048/2133_-_Driving-128.png");
        return driver;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_submit && validName(driverName.getEditText().getText().toString())) {
            Intent intent = new Intent(this, MainActivity.class);
            Driver driver = createDriver();
            intent.putExtra(Constants.EXTRA_DRIVER, driver);
            prefs.putDriver(Constants.DRIVER_KEY, driver);
            startActivity(intent);
            this.finish();
        }
    }

    public void onItemSelected(AdapterView adapterview, View view, int i, long l) {
        switch (view.getId()) {
            case R.id.choose_area:
                route.setEnabled(true);
                route.setAdapter(simpleSpinnerAdapter(Utils.getRoutesForArea(area.getSelectedItem().toString(), getApplicationContext())));
                break;
            case R.id.choose_route:
                submit.setEnabled(true);
                break;
        }
    }

    public void onNothingSelected(AdapterView adapterview) {
    }

}
