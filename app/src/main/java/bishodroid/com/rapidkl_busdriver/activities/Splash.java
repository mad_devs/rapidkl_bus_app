// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package bishodroid.com.rapidkl_busdriver.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import bishodroid.com.rapidkl_busdriver.R;
import bishodroid.com.rapidkl_busdriver.pojo.Driver;
import bishodroid.com.rapidkl_busdriver.utils.Constants;
import bishodroid.com.rapidkl_busdriver.utils.Preferences;

public class Splash extends AppCompatActivity {

    private Preferences prefs;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_splash);
        prefs = Preferences.getInstance(getApplicationContext());
        Thread timer = new Thread() {


            public void run() {
                try {
                    sleep(3000L);
                } catch (InterruptedException interruptedexception) {
                    interruptedexception.printStackTrace();
                } finally {
                    Driver driver = prefs.getDriver(Constants.DRIVER_KEY, null);
                    if (driver != null) {
                        Intent i = new Intent();
                        i.putExtra(Constants.EXTRA_DRIVER, driver);
                        startActivity(new Intent(Splash.this, MainActivity.class));
                        finish();
                    } else {
                        startActivity(new Intent(Splash.this, ChooseBusRoute.class));
                        finish();
                    }
                }
            }

        };
        timer.start();
    }
}
