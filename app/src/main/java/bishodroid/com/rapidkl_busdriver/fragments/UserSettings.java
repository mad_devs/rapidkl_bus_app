package bishodroid.com.rapidkl_busdriver.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.google.gson.Gson;

import bishodroid.com.rapidkl_busdriver.R;
import bishodroid.com.rapidkl_busdriver.pojo.Driver;
import bishodroid.com.rapidkl_busdriver.utils.Constants;
import bishodroid.com.rapidkl_busdriver.utils.Preferences;

/**
 * Created by Bisho on 21/2/2016.
 */

public class UserSettings extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String KEY_USERNAME = "prefs_driver_name";

    private Preferences prefs;
    private Gson gson;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.user_settings);
        prefs = Preferences.getInstance(getActivity().getApplicationContext());
        gson = new Gson();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPrefs, String key) {
        switch (key) {
            case KEY_USERNAME:
                Driver driver = prefs.getDriver(Constants.DRIVER_KEY, null);
                driver.setName(sharedPrefs.getString(KEY_USERNAME, null));
                prefs.putDriver(Constants.DRIVER_KEY, driver);
                break;
        }
    }
}