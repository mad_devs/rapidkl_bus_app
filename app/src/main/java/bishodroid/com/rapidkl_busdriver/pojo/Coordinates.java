package bishodroid.com.rapidkl_busdriver.pojo;

/**
 * Created by Bisho on 21/2/2016.
 */
public class Coordinates {

    private double longitude;
    private double latitude;
    private String updateTime;

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "Coordinates{" +
                "longitude=" + longitude +
                ", latitude=" + latitude +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }
}
